---
title: Writing in the Sciences
subtitle: "A short overview of the course by Stanford University available in Coursera"
date: 2021-07-23
tags: ["writing"]
toc: true
---

I've recently started following the [Writing in the Sciences course](https://www.coursera.org/learn/sciwrite/home/welcome) offered by Dr. Kristin Sainani from Stanford University.  

Professional writers know most, if not all, of the recommendations from this course. You might already encounter some of these recommendations in style guides, like the one of Microsoft of Apple and especially in the [Technical writing course by Google](https://developers.google.com/tech-writing/overview). 

Still, I want to recap a few pieces of advice from this course.
I will not repeat the very basic principles: remove empty filler words, use active voice instead of passive voice, cut unnecessarily long sentences into simpler and shorter ones.

### Eliminate 'there is/there are'

They are often superfluous.
The data confirm that there is an association between vegetables and cancer. -> The data confirm an association between vegetables and cancer. 

### Turn negative into positive

> She did not want to perform the experiment incorrectly.
She wanted to perform the experiment correctly.

She was not often right -> She was usually wrong.

It's usually much clearer when you use the positive construction. A double negative can be especially confusing.
Some examples:
+ does not have -> lack
+ did not pay attention to -> ignored
+ did not succeed -> failed

### Use strong verbs

The WHO reports that approximately two-thirds of -> The WHO estimates that two-thirds...


### Don't kill verbs by turning them into nouns

There is a problem of turning verbs into nouns. Nouns slow down the reader.  
Take an assessment of -> assess. 
Provide a description of -> describe.

### Don't bury the main verb

When a subject is "declared", the reader always wait for the verb!
An when the main verb is far from the subject, it becomes difficult to make sense of a sentence.

One **study** of 930 adults with multiple sclerosis (MS) receiving care and one of two managed care settings or in a fee-for-service setting **found** that only two-thirds of those needing to contact a neurologist for an MS-related problem in the prior 6 months had done so.

One **study found** that, of 930 adults with multiple sclerosis (MS) who were receiving care in one of two managed care settings or in a fee for service setting, only two-thirds of those needing to contact a neurologist for an MS-related problem in the prior six months had done so.

### Use colon, dashes, semicolons, and parenthesis!

They make sentences more efficient and less monotonous to read.  
When only a comma is used to separate independent clauses in a sentence, this can often lead to run-on sentences.
Besides separating, the dash can only serve to emphasize a phrase.

"The drugs also triggered overweight mice to shed significant amounts of fat—up to half their body weight.

### 1 paragraph = 1 idea

This rule is well known among professional writers.
"Short paragraphs are not only more focused, but they provide a lot of white space on the page. Readers appreciate white space.".
Huge blocks of text with no breaks are difficult to read.  

Dr. Kristin Sainani says that while editing students' work, she often ends up splitting their paragraphs.


### Give away the punch line early

Many people like to provide all sorts of details before reaching for a conclusion.
If you give away the conclusion first, you let the reader know where you're going. This style is also known as the [inverted pyramid](https://en.wikipedia.org/wiki/Inverted_pyramid_(journalism)).

### Do not overuse transition words

If your writing flow logically, and your reader is able to follow your logic, you don't need to put many transition words. If the logic is not sound, transition words will not fix it. If you have to use them, a simple **but** is often better than fancy words like "nevertheless" or "on the other hand".
"But" and "however" are used when a writer need to signal a change of direction. Other transition words, such as "furthermore" or "hence", are used to continue in the same direction, so you can try to remove them. 

"Despite what you may have been taught along the way, repeating a word is not a cardinal sin".
Synonyms can lead to confusions. The readers may think you are talking about something new.

P.S. Clear thinking becomes clear writing; one can’t exist without the other.
(William Zinsser. On Writing Well)








