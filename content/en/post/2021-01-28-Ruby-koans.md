---
title: Learning Ruby through koans
subtitle: "The Koans walk you along the path to enlightenment in order to learn Ruby."
date: 2021-02-04
tags: ["ruby"]
toc: true
---

The koans are a way to learn a programming languages through testing. The koans let you think about the questions, problems.
The [Ruby koans](http://rubykoans.com) were initially conceived by Jim Weirich.  
If you are absolutely new to programming, the koans may seem hard to start with because they requires some self-study and understanding the basics of programming.

In this article we'll follow the "path to enlightenment". Each section is named after the koan filename.

### about_asserts
Because koans are based on testing, in the very first lesson, we start by learning the `assert` module. Just like in C, Python (and certainly in many other programming languages), `assert` is used to check whether a statement (assertion) is true. Example:
```
assert_equal __(2), 1 + 1
```

### about_nil  
Ruby often says that `nil` is also an object.
```
  def test_nil_is_an_object
    assert_equal true, nil.is_a?(Object), "Unlike NULL in other languages"
  end
```
The koan lets the reader think about the following: what is better to use `obj.nil?`or `obj == nil`.

### about_objects
Object is a key concept in Ruby. We often hear that "Ruby is a pure object-oriented language" and that "Everything in Ruby is an object". So Objects are a key notion in Ruby. This koan shows that things like `1`, `"string"`, `nil`, and even `Object` are all Objects:
```
    assert_equal true, 1.is_a?(Object)
    assert_equal true, 1.5.is_a?(Object)
    assert_equal true, "string".is_a?(Object)
    assert_equal true, nil.is_a?(Object)
    assert_equal true, Object.is_a?(Object)
```

The koan also introduces the `object_id` property, and we learn that small integers have their reserved IDs like `2.object_id == 5`.

### about_arrays
We learn how to create arrays and access their members. We also encounter, for the first time, the `Range`.  

The [left shift operator](https://en.wikipedia.org/wiki/Bitwise_operation#C-family) can be used to add elements to arrays:
```
array << 333
```

### about_array_assignment
We learn how to assign values to variables using arrays. This includes multiple (or "parallel") assignments such as `first_name, last_name = ["John", "Smith"]`. Parallel assignment has some tricky moments when the array length is not equal to the number of provided variables. So we have to remember how to deal with cases such as:
```
first_name, last_name = ["Cher"]
```
In this particular example, the value of the `last_name` will be `nil`.


### about_hashes
Hashes in Ruby are similar to Python dictionaries or JavaScript objects. They also consist of key-value pairs between curly braces.  
The koan shows two ways to access a hash values: using brackets like `hash[:one]` or using the `fetch` method. And it asks "Why might you want to use #fetch instead of #[] when accessing hash keys?". The difference is that, with brackets, `nil` is returned if a key is not found. While with `fetch` you the "KeyError" is raised, or you can specify a default value.  
Later on, we are taught that you can set default values for missing keys when we create a hash: `hash2 = Hash.new("default")`.
But if you assign a value to a non-existing key, this will mutate the defaults:
```
irb(main):036:0> hash = Hash.new([])
=> {}
irb(main):038:0> hash[:one] << "uno"    # :one does not exist so "uno" is added to the default value
=> ["uno"]
irb(main):039:0> hash[:two] << "dos"     # :two does not exist so "dos" is added to the default value
=> ["uno", "dos"]
irb(main):040:0> hash[:two]                    # this will display the default value
=> ["uno", "dos"]
irb(main):041:0> hash                                # but note that the hash itself is not modified
=> {}
```
### about_strings
Strings are single-quoted or double-quoted characters. When you want to have both single and double quotes in a string, you can create this string using the "Perl-inspired way": a percent sign followed by a pair of parenthesis, brackets, curly braces, or any other non-alphanumeric character: ` a = %(flexible quotes can handle both ' and " characters)`. This is one of the ways to avoid the [leaning toothpick syndrome](https://en.wikipedia.org/wiki/Leaning_toothpick_syndrome).  
Besides a concatenation of strings with `+` you can also use the "shovel operator" `<<` just like with arrays. This will alter the original string, so you don't need to create a new variable.
```
irb(main):001:0> hi = "Hello, "
irb(main):002:0> hi << "World"
irb(main):003:0> hi
=> "Hello, World"
```
With double quotes, you can use string interpolation. Single quotes do not interpolate:
```
irb(main):001:0> string = "The square root of 25 is #{Math.sqrt(25)}"
=> "The square root of 25 is 5.0"
irb(main):002:0> string = 'The square root of 25 is #{Math.sqrt(25)}'
=> "The square root of 25 is \#{Math.sqrt(25)}"
```

### about_symbols
People sometimes don't understand when and why to use Symbols. In fact, not so many programming languages support the Symbol data type. JavaScript introduced [Symbols](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol) in ES6. Python does not have it at all. A famous Ruby programmer and writer [_why](https://en.wikipedia.org/wiki/Why_the_lucky_stiff) gave the following explanation: "Usually, symbols are used in situations where you need a string but you won’t be printing it to the screen".  
The koan concentrates on the fact that symbols are not just immutable strings. Symbols do not have string methods. And symbols are unique: two variables referring to the same symbol (unlike strings) will have the same `object_id`:
```
    symbol1 = :a_symbol
    symbol2 = :a_symbol
    assert_equal true, symbol1 == symbol2
```

### about_regular_expressions
Ruby regular expressions are usually created using `/.../`. The syntax is said to be inspired by Perl regex.  
A character class can be negated using the tab character `^` or capital letters:
```
assert_equal "the number is", "the number is 42"[/[^0-9]+/]
assert_equal "the number is ", "the number is 42"[/\D+/]
```
Think about:
why we call the repetition operators "greedy"?
Explain the difference between a character class ([...]) and alternation (|).

### about_methods
Methods in Ruby are what is often referred in other languages as functions. This koans demonstrates that you can call a method with or without parentheses.
```
irb(main):001:0> def my_global_method(a,b)
irb(main):002:1>   a + b
irb(main):003:1> end
=> :my_global_method
irb(main):008:0> my_global_method(2,3)
=> 5
irb(main):009:0> my_global_method 2,3
=> 5
```
This differs Ruby from Python: in Ruby *There is more than one way to do it* (like in Perl); in Python, in contrast, *There should be one — and preferably only one — obvious way to do it*.[^1]  

The koan provides examples of "implicit return" meaning that if, in a method, you do not write `return` explicitly, Ruby will return the value of the last instruction in this method. Again, if we compare with Python, the latter is stricter: a function without return will always return `None`.

### about_keyword_arguments

This very short koan is about keyword arguments also known as [named arguments](https://en.wikipedia.org/wiki/Named_parameter). Keyword arguments can be combined with mandatory arguments. Think about it: "Keyword arguments always have a default value, making them optional to the caller". 

### about_constants

If a constant is declared at the top level of a Ruby program, it belongs to the main [Object](https://ruby-doc.org/core-3.0.0/Object.html) class. Modules and classes can access the constants defined in their parents. They can be referenced by double colons: `::C`.
QUESTION: Which has precedence: The constant in the lexical scope or the constant from the inheritance hierarchy? 

### about_control_statements

If-then-else statements are examples of control statements.  
Ruby also has the `unless` statement: `unless false` is the same as saying `if !false`, which evaluates as `if true`.  
To skip iteration, Ruby uses the `next` keyword. This is known as `continue` in C, Java, JavaScript, and Python.

### about_true_and_false
In Ruby, `nil` is treated as `false` too. In fact, only false and nil are "falsey", everything else is "truthy".  
Zeros, empty strings, empty arrays, and empty hashes are all truthy.

```
  def test_everything_else_is_treated_as_true
    assert_equal  :true_stuff, truth_value(1)
    assert_equal  :true_stuff, truth_value(0)
    assert_equal  :true_stuff, truth_value([])
    assert_equal  :true_stuff, truth_value({})
    assert_equal  :true_stuff, truth_value("Strings")
    assert_equal  :true_stuff, truth_value("")
  end
```

### about_triangle_project

This is a small project, which ask us to complete writing the existing `triangle` method.

```
# It returns:
#   :equilateral  if all sides are equal
#   :isosceles    if exactly 2 sides are equal
#   :scalene      if no sides are equal

def triangle(a, b, c)
  if a == b && b == c
    return :equilateral
  elsif a == b || b == c || a == c
    return :isosceles
  else
    :scalene
  end
end
```



[^1]: This principle reminds me of a good example provided by David Heinemeier Hansson in the [Rails Doctrine](https://rubyonrails.org/doctrine/): *"Ruby accepts both exit and quit to accommodate the programmer’s obvious desire to quit its interactive console. Python, on the other hand, pedantically instructs the programmer how to properly do what’s requested, even though it obviously knows what is meant (since it’s displaying the error message)."*
