---
title: Programming materials
subtitle: That I liked
date: 2020-12-01
tags: ["ruby"]
toc: true
---

I like finding interesting materials to learn programming. Besides just the pleasure of learning something new, this gives me, as a writer, two important opportunities:<!--more-->
+ Better understand developers. No matter what language, framework, or technology you learn, many terms from the development world are interrelated: you will often hear *classes, objects, methods* or *compiler, data type* or *deployment, unit test*, and so on. Word usage vary but the more I study them, the more I find some similarities.
+ Analyze *how* the material is written and designed. The approaches to deliver content, images, and examples are very different in each company. I'm very curious to see how the documentation or a course or a blog can be organized.

Here I'd like to list some of the resources I am thankful to for their way of delivering information and their ability to catch my attention and thus—teach me so much.  
Ability to show a working code example is a MUST in 2020, at least for web development courses.

### W3School
![W3school image](/img/w3school.png#smallimage)
**Site**: https://www.w3schools.com  
**Stacks**: HTML, XML, CSS, JavaScrpipt, Python, PHP, Bootstrap, JQuery, SQL, Node.js, React, and much more.  
**Plans**: *"W3Schools is, and will always be, a completely free developers resource."* (you can buy certificates)  

<img src="img_girl.jpg" alt="Girl in a jacket" width="500" height="600">

#### Pros
Number one in my list. Incredible source of information for web development.  
There are various types of material: **References** (formal descriptions of syntax, tags, functions), **Tutorials** (step-by-step guides with exercises), and **How to**'s where they give some close-to-real-life examples for web development, such as how to design responsive navbars or create a basic password-validation form.  
Without any doubt, the site is recommended to anyone who starts web development or who looks for a particular use case or implementation example.  

Not to forget to mention: all the examples are provided with the code playground, which makes it much easier to try the theory yourself.

#### Cons
Hard to find a con.

### SoloLearn

Site: https://www.sololearn.com/  

Stacks: Web (HTML, CSS, JavaScrpipt, JQuery courses), C, C++, C#, Java, Python, Ruby, PHP, Swift, frameworks (Angular, React), Data Science.  


Plans: free account required for the use, can be upgraded to PRO.  

#### Pros

+ Quick way to learn basics of a language using short courses and exercises.
+ Has **Code Coach** where you can solve problems using your favorite language (I used Python and Ruby).  
+ A great feature is the **Comments** section under each lesson and exercise. Comment often have useful additional info or interesting discussions.
+ Of course, almost every lesson has the **Try it yourself** code playground.

#### Cons

Users sometimes complain that one or another aspect is not explained in enough details. This is understandable: the app is designed, first of all, for quick learning of basics. Mobile apps seldom have ten-pages texts or many external links.

### Try Ruby
Site: https://try.ruby-lang.org/
Language: Ruby

Initially created by _why the lucky stiff, it is now supported by Ivo Herweijer.
