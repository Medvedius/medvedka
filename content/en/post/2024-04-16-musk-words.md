---
title: New words learned in Elon Musk biography
subtitle: "In 5 minutes!"
date: 2024-04-16
toc: true
---

### To rummage (verb)

[Definition](https://www.merriam-webster.com/dictionary/rummage)

- to make a thorough search or investigation
- to engage in an undirected or haphazard search

Quote from the book

> Kids in the 1970s and earlier grew up rummaging under the hoods of cars

### Lofty (adj)

[Definition](https://www.merriam-webster.com/dictionary/lofty). 
- Elevated in character and spirit, noble (lofty ideals)
- Elevated in status, superior (the less lofty customers of the bar)

Quote from the book:
> Musk now had a new mission, one that was loftier than launching an internet bank or digital Yellow Pages (Chapter 14, Mars)

### Ouster (noun)

[Definition](https://www.merriam-webster.com/dictionary/ouster): a judgment removing an officer, expulsion

Quote from the book:
> After his ouster from PayPal, Musk bought a single-engine turboprop and decided to learn how to fly.
