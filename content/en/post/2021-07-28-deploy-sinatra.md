---
title: Deploy a Sinatra-based app on Heroku
subtitle: "In 5 minutes!"
date: 2021-07-28
toc: false
---

How to quickly deploy a web application created using the Sinatra framework.


### Before we start

Before we go to Heroku, I assume that:
+ The source code of your Sinatra app is stored in GitHub.
+ You have a `config.ru` Rack configuration file.
If you don't have this file yet, create it in your app repository and add a similar content:
```ruby
require './hello' # this is the main file of your app
run Sinatra::Application
```

### How-to

Now, we can proceed to deploy:

1. Create a [Heroku account](https://signup.heroku.com) if you don't have one.
2. On the main page, click **New** → **Create new app**.
3. On the **Create New App** page that open, enter you app name and select your app region. The name will be a part of the app URL: `<name>.herokuapp.com`.
4. Click **Create app**. You are then redirected to your app settings page.
5. Under **Deployment method**, click **GitHub**.  
  The **Connect to GitHub** section appears below. If this is the first time you connect to GitHub through Heroku, you may be prompted to authorize "Heroku Dashboard" to access your account.
6. In the **repo-name** box, enter the name of your GitHub repository where the source code of your app resides.
![Connect GitHub](/img/heroku_connect_github.jpeg)
7. Click **Search** and once the repository is found, click **Connect**.
 In fact, that's all, you are now able to deploy your Sinatra app in one click
 8. Under **Manual deploy**, click **Deploy Branch**.
 You'll see the build progress and, if everything is OK, a green check mark. You can then click **View** to open your app web interface.

![View app](/img/heroku_view_app.jpeg)


### Getting more

Besides just deploying, Heroku has a ton of useful features. What I immediately used was checking the app logs. To view logs, you need to install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli) locally. You'll then have access to Heroku CLI commands.

+ Show the logs of your app (the logs are very well written and already helped me find some errors):
```ssh
heroku logs --tail -a myapp
```
and the output will be similar to:
```bash
2021-07-28T20:17:51.800058+00:00 heroku[web.1]: Starting process with command `bundle exec rackup config.ru -p ${PORT:-5000}`
2021-07-28T20:17:57.110636+00:00 app[web.1]: Puma starting in single mode...
2021-07-28T20:17:57.116667+00:00 app[web.1]: * Puma version: 5.3.2 (ruby 2.7.2-p137) ("Sweetnighter")
```

+ Open you app in the browser (support you don't remember the URL):
```
heroku open -a myapp
```

+ Show the [release](https://devcenter.heroku.com/articles/releases) history of your app:
```
heroku releases -a myapp
```
and the output will be similar to:
```
=== myappReleases - Current: v7
v7  Deploy b0a502e3            lorddarthvader@example.com  2021/07/28 23:17:49 +0300 (~ 41m ago)
v6  Deploy b0a502e3            lorddarthvader@example.com  2021/07/28 17:10:37 +0300 (~ 6h ago)
v5  Deploy aa7af25b            lorddarthvader@example.com 2021/07/28 17:08:09 +0300 (~ 6h ago)
```






