---
title: Theme modifications
subtitle: What I changed in the initial BeautifulHugo theme
comments: true
---
<!--more-->
<details>
<summary>Summary length</summary>

The post "summaries" (shown on the post list before you open the post) were long for me. 70 words, I guess, which is the default length.
I found that this can be customized using the [summaryLength](https://gohugo.io/content-management/summaries/#automatic-summary-splitting) Hugo variable.
I've added this to my `config.toml`:
```
summaryLength = 10
```
First, it did not work and I learned that I should put it above permalinks section.
Hugo does not cut a phrase on ten words exactly. It rather let the phrase to be finished (using the dot), which I found very useful.  

You can also manually add `<!--more-->` to your post if you want to divide it in a specific place. If both options are used, `<!--more-->` prevails.
</details>
<details>
<summary>Image class</summary>

How to style an image in a Markdown post provided that it will be processed by Goldmark?  

In Jekyll (thanks to [kramdown](https://kramdown.gettalong.org/quickref.html)), you can add CSS classes to any element of your Markdown text.  
Hugo (as of version 0.74.3) allows adding classes to headings only. So I could not simply add a class to an image in a post.  
One of the ways to solve it is using a [CSS [attribute*=value] Selector](https://www.w3schools.com/cssref/sel_attr_contain.asp) where the `value` would be any string placed just after the number sign `#` (*URI fragment*). So we proceed as follows:
1. Add URI fragment to the image path like:  
`![New image](/img/w3school.png#smallimage)`. 
2. Add a CSS selector in `static/css/main.css`:
```
img[src*="#smallimage"] {
   width:50px;
   height:50px;
}
```
Is there a more elegant way to achieve this? I don't know of it. Using raw HTML tags is certainly **less** elegant.
</details>
<details>
<summary>Changes section</summary>

I've added an icon from the FontAwesome 5 free pack to the blog top navigation bar. Clicking the icon will lead to **this** page.
I added it using the `config.toml` file by creating a new `[[menu.main]]` entry with a `pre` parameter:
```
[[menu.main]]
    name = "Changes"
    url = "/page/changes"
    pre = "<i class='fas fa-chess fa-lg'></i>"
    weight = 5
```
The `nav.html` template will receive the `.Pre` variable meaning a menu entry can be prefixed by an icon if it is set in front matter.
```
<a title="{{ .Name }}" href="{{ .URL  | relLangURL }}">{{ .Pre }} {{ i18n .Name }}</a>
```

As far as it is a multilingual blog, all menu entries are translated using the [i18n function](https://gohugo.io/functions/i18n/) function. I don't need text here, just an icon, so I just don't add `Changes` to i18n files.  
However, this icon is still available in all languages, must fix it later.
</details>
<details>
<summary>Time to read</summary>

Hugo has built-in `.ReadingTime` and `.WordCount` page variables. So, it is very easy to add this info to the blog posts:
```
    {{ if (eq .Section "post") }}
<span>{{ i18n "TimeToRead" }}: {{ .ReadingTime }} {{ i18n "minute" }} ({{ .WordCount }} {{ i18n "words"}}).</span>{{end}}
```
To support this new text in posts, I also had to add three localization keys to my `i18n` files:  `TimeToRead`, `minute`, and `words`. A small issue made GitLab build pipeline fail: YAML [does not support tabs](https://yaml.org/faq.html)! I seems that I accidentally copied tabs from an external example. I replaced them with spaces, which fixed the error. The final result can look like this:
```
Read time:  7 minutes (1476 words).
読む時間: 7 分 (1476 言葉)
```
</details>
<details>
<summary>Disqus comments</summary>

Hugo says it [supports Disqus comments](https://gohugo.io/content-management/comments/) out of the box using the "internal templates".  
The Beautiful Hugo theme says it also [supports Disqus comments](https://themes.gohugo.io/beautifulhugo/), you just have to specify you `disqusShortname`.  

Both statements did not work for me, I tried several times. So, what I did is just copied the [code snippet](https://disqus.com/admin/install/platforms/universalcode) from the Disqus docs, which I've already used in Jekyll. And it seemed to work.
</details>

