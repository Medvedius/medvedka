---
title: Learning Norsk
subtitle: Some interesting things I discovered about Bokmål
date: 2021-02-20
tags: ["language"]
toc: true
---

I learn the Norwegian language (Bokmål) in Duolinguo.
Here are some things I found surprising in Norwegian.

### Feminine articles

The Norwegian language has articles. As in many languages (but not in English), the choice of an article depends on the grammatical gender of the word. In Norwegian, you have **en** for masculine, **ei** for feminine, and **et** for neuter. However, as Duolinguo says, "All feminine gendered nouns can be classified as masculine gender as well". Meaning you can write **ei kvinne** as well as **en kvinne**. "The choice really is up to you".  
Hmm... For me, as a beginner, this sounds confusing. Do I need to learn which words have a feminine gender, or I can just always use the masculine articles provided it is not a neuter word? I don't remember such flexibility in other languages I learned: English, French, German, or Japanese. If you have similar examples, please tell me or write it in comments.

### Definite articles

> The definite form is formed by placing the indefinite article, "a/an", or in Norwegian, "en/et", at the end of the word instead of at the beginning.  

Examples: et barn = a child, barnet = the child. Very convenient, I should admit. ☺ No need to learn new articles.  
For feminine words, you can use two different endings: **kvinna** or **kvinnen** meaning "the woman", **jenta** or **jenten** meaning "the girl".

### Verb conjugation

> Conjugation couldn't be simpler.

This is true! I always thought that English verb conjugation in the present simple tense was very simple: you only have to remember the third person singular. Today I learned that some languages are even simpler. In Norwegian, you have a single form for all persons. Even the verb "to be" follows this rule.

| Singular          | Plural|
| ------------- |:-------------:| -----:| -----:|
| jeg er     | I am | vi er | we are|
| du er   | you are      |   dere er | you are|
| han, hun, det er| he, she, it is    |   de er | they are|

Apparently, Danish and Swedish also follow this pattern (at least in the present tense).

### Denne/den and dette/det

These are demonstrative pronouns. Their usage depends on the distance between a speaker and the object.
Denne koppen = this cup, den koppen = that cup. In French, there are "celui-ci" and "celui-là" but they are used a bit differently.
The Japanese language seems to have something similar: you say この for something close to the speaker, その close to the listener, and あの far from both.

| English          | Norwegian| French | Japanese
| ------------- |-------------| -----| -----|
| This book     | Denne boken | Ce livre| この本| 
| That book   | Den boken| Ce livre | その本|
| These books| Disse bokene|  Ces livres | これらの本
| Those books| De bokene|   Ces livres| それらの本

### Ja, takk

> En kaffe, takk!
> A coffee, thanks!

["Ja takk"](https://forum.duolingo.com/comment/16980389/Ja-takk) can be translated as "Yes, thank you" or "Yes please". o_O  

[Some say](https://www.lifeinnorway.net/how-to-say-please-without-saying-please/) that Norwegians "don't even have a word for “please”". I don't agree with that: they have several ways to say "please" and using "takk" (thank you) is one of them. "Vær så snill" is a more "nice" version of "please".

### På

> På has as many uses and translations as there are types of brunost in Norway

"På" seems to be a very frequent preposition in Norwegian. Maybe like "in" in English. For now, I've learned the following examples of usage:
+ Location. Hun sitter på stolen = She is sitting on the chair.
+ Language. Hva heter det på russisk? = What is that (called) in Russian?

### Counting

> There is no tricky system or crazy multiplication weirdness.

Is this an allusion to the French language? ☺


### Possessive Pronouns

Norwegian possessive pronouns may be placed either before or after the noun. When a pronoun is placed before, a noun is used in the indefinite form. When a pronoun is placed after, a noun is used in the definite form.

| Before (indefinite)  | After (definite)|
| ------------- |-------------| 
| min far    | faren min |
| din far    | faren din |
| hans far    | faren hans |
| mi mor  | mora mi |
| mitt barn| barnet mitt |
| mine foreldre| foreldrene mine|

The after + definite is "the more common form in colloquial Norwegian". The indefinite form "places special emphasis on the possessor".  
Another interesting thing: **deres** can mean both "your" and "their".


### Prepositions of possession

They are often omitted!  

| Norwegian | 	English |
| -----------------|--------------|
| en kopp kaffe |	a cup of coffee |
| et glass vann |	a glass of water |