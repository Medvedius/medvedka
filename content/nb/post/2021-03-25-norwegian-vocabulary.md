---
title: Learning Norsk, part 2 (Vocabulary)
subtitle: Interesting vocabulary I learned from Duolinguo
date: 2021-03-25
tags: ["language"]
---

I learn the Norwegian language (Bokmål) in Duolinguo.
 
### Brunost
Gender: m
[Brunost](https://en.wikipedia.org/wiki/Brunost) is a Norwegian brown cheese.

### Hytte
Gender: f
Meaning: cabin
In the second or third lesson, I encountered the word [hytte](https://www.google.com/search?q=norwegian+hytte&client=safari&rls=en&sxsrf=ALeKk01ebK_Nn-tjadgFwMOoBOyc3c5p8g:1616700188457&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjurMPDlczvAhVCmYsKHRuEC0YQ_AUoAXoECAEQAw&biw=1352&bih=637) meaning "cabin". That was a bit strange: we only started learning the language, does this word is really so important? Apparently, yes ☺. Many Norwegian people own these holiday houses, often in rural area.

### Brus
Gender: m
Meaning: soda, pop, fizzy drink
The first word I encountered in **The Cafe** lesson was "en brus". Maybe Norwegians like soda?

### Similar words

| Norsk     | svenska    | danske    | English   | Russian  | German    |
|-----------|------------|-----------|-----------|----------|-----------|
| Tallerken | Tallrik    | Tallerken | Plate     | Тарелка  | Teller    |
| Billett   | Biljett    | Billet    | Ticket    | Билет    | Ticket    |
| Solen     | Solen      | Solen     | The sun   | Солнце   | Die Sonne |
| Appelsin  | Apelsin    | Appelsin  | Orange    | Апельсин | Orange    |
| Et kontor | Ett kontor | Et kontor | An office | Офис     | Ein Büro  |
| Et basseng| En pool | En pool | A pool | Бассейн | Ein Pool
| En butikk | En butik | En butik | A shop | Магазин | Ein Geschäft


