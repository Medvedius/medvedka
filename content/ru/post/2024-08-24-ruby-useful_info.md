---
title: Ruby useful info and commands
subtitle: Useful info
date: 2024-08-24
tags: ["example"]
toc: true
comments: true
---

### Database in Rails

```shell
bin/rails db:drop db:create db:migrate
``` 

Drop the database, recreate it, and make migration. 
For non-production mainly. Useful when a new constraint is added but existing data doesn't have it.

```shell
rails db:seed
```

Add [initial data](https://en.wikipedia.org/wiki/Database_seeding) to the database.
It takes data from `db/seeds.rb`, which can contain code liks
`User.create(username: 'admin', password: 'password')`.

Alternatively, you can use fixtures to seed data: `bin/rails db:fixtures:load`. For example:
```ruby
# db/seeds.rb

puts "\n== Seeding the database with fixtures =="
system("bin/rails db:fixtures:load")
```

### JavaScript

```shell
bin/rails generate stimulus controllerName
```
Generates a new [Stimulus](https://stimulus.hotwired.dev/handbook/introduction) controller
and saves in as a `app/javascript/controllers/#{controller_name}_controller.js`.
It also updates **controllers/index.js** accordingly using the `stimulus:manifest:update` Rake task.
For example:

```Javascript
// app/javascript/controllers/index.js
import ChatController from "./chat_controller"
application.register("chat", ChatController)
```

<br>

### User Interface

[Simple_form](https://github.com/heartcombo/simple_form) provides components to create forms.
