---
title: Vocabulary
subtitle: Новые слова и выражения
date: 2024-08-06
toc: true
---

**Pen**: 
+ a place of confinement for persons held in lawful custody
+ an enclosure with an open framework for keeping animals
A horse pen — загон для лошадей

**Bloomery** is a type of metallurgical furnace once used widely for smelting iron from its oxides.
Сыродутная печь (сыродутный горн) — один из первых в истории металлургических агрегатов для получения металлического железа из руды путём химического восстановления.

**Bellows** or pair of bellows is a device constructed to furnish a strong blast of air.
Мехи́, или Меха — поддувальный снаряд для кузниц. Человек, приставленный к кузнечным или органным мехам для дутья — меходуй.

**Gobsmacked** : overwhelmed with wonder, surprise, or shock. Ошеломленный

**To rescind**: to make void by action of the enacting authority or a superior authority, _repeal_. 
Rescind your appointment as Marshal — Отменяю ваше назначение на пост маршала.

**Antic** (n) — an attention-drawing, often wildly playful or funny act or action.
Antic (adj) — characterized by clownish extravagance or absurdity
> childish antics
> The drunken antics of my courtier Xenia have been the subject of court gossip.

**To maim** (v) — to mutilate, disfigure or wound seriously. Искалечить.

**Jitters** a sense of panic or extreme nervousness. _Wedding jitters_.