---
title: Asciidoctor
subtitle: Знакомство с ещё одним инструментом создания документации
date: 2021-03-30
tags: ["docs"]
toc: true
draft: false
---

### Введение

[Asciidoctor](https://asciidoctor.org) — это инструмент для создания документации и конвертации текстов AsciiDoc в HTML5, DocBook, PDF, EPUB3 и другие форматы.  
AsciiDoc в свою очередь является языком разметки в том числе для создания документации в формате DocBook.
Первый написан на Ruby, а второй на Python.  
> Asciidoctor is a drop-in replacement for its predecessor, AsciiDoc Python (asciidoc.py).

Сегодня попробую изучить их.
Установка на macOS 11.2.3 `brew install asciidoctor` закончилась ошибкой, запускаю через rosetta:
```
anton@MacBook-Air-Anton arch -x86_64 brew install asciidoctor
==> Downloading https://homebrew.bintray.com/bottles/asciidoctor-2.0.12.big_sur.bottle.t
==> Downloading from https://d29vzk4ow07wi7.cloudfront.net/4af4798f8081100713a1b3d301107
######################################################################## 100.0%
==> Pouring asciidoctor-2.0.12.big_sur.bottle.tar.gz
🍺  /usr/local/Cellar/asciidoctor/2.0.12: 2,980 files, 20.7MB
```
Теперь всё в порядке. Проверяю версию:
```
anton@MacBook-Air-Anton old % asciidoctor -v
Asciidoctor 2.0.12 [https://asciidoctor.org]
Runtime Environment (ruby 2.6.3p62 (2019-04-16 revision 67580) [universal.arm64e-darwin20]) (lc:UTF-8 fs:UTF-8 in:UTF-8 ex:UTF-8)
```
Пробую создать обычный текстовый документ с расширением `.adoc`.
Запускаю создание HTML-файла:
```
asciidoctor hi.adoc
```
Указание формата не требуется, так как HTML считается форматом по умолчанию.
По умолчанию для оформления HTML используется некая дефолтная тема, которую [можно кастомизировать](https://docs.asciidoctor.org/asciidoctor/latest/html-backend/default-stylesheet/#customize-the-default-stylesheet) или использовать один из существующих шаблонов ([skins](https://github.com/darshandsoni/asciidoctor-skins)).
Дефолтная тема находилась у меня в каталоге `/Users/anton/.rvm/gems/ruby-2.7.2/gems/asciidoctor-2.0.12/data/stylesheets`.
Пробую сконвертировать в DocBook:
```
asciidoctor -v docbook5 hi.adoc
```
В результате создаётся файл `hi.xml`.

Ну а для того чтобы сконвертировать AsciiDoc-документ в PDF требуется установить [asciidoctor-pdf]( https://asciidoctor.org/docs/asciidoctor-pdf/). И тут начались пляски с установкой на чип M1 Apple Silicon.
В итоге вроде бы удалось установить rvm и нужную версию Ruby (отличную от системной, поставляемой по умолчанию в macOS).
Компонент (gem) asciidoctor-pdf] основан на библиотеке [Prawn](https://prawnpdf.org/api-docs/2.3.0/), которая ставится вместе с ним.
```
anton@MacBook-Air-Anton ~ % asciidoctor-pdf -v
Asciidoctor PDF 1.5.4 using Asciidoctor 2.0.12 [https://asciidoctor.org]
Runtime Environment (ruby 2.7.2p137 (2020-10-01 revision 5445e04352) [arm64-darwin20]) (lc:UTF-8 fs:UTF-8 in:UTF-8 ex:UTF-8)
```
Далее берём пример adoc-документа и запускаем создание PDF-файла командой `asciidoctor-pdf pdf-example.adoc`.
Получилось красиво!

Теперь попробуем поддержать документ на другом языке. В Asciidoctor есть [Localization Support](https://docs.asciidoctor.org/asciidoctor/latest/localization-support/), что позволяет выполнить перевод некоторых предустановленных строк, таких как Chapter, Figure, Example, Table of Contents.




