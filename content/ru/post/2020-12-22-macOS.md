---
title: Первая неделя на macOS
subtitle: На примере MacBook Air 2020 M1
date: 2020-12-22
tags: ["example"]
toc: true
draft: false
---

### Введение

Мой Samsung довольно стар, появилось желание поменять компьютер.  
При этом хотелось попробовать что-то новое. До этого использовал Windows 7 и 8, Debian, Ubuntu, Xubuntu и даже (однажды по работе) — отечественную Astra Linux.

MacBook я хотел купить ещё полтора года назад, но тогда почему-то отложил. И как оказалось не зря: 10 ноября 2020 Apple официально анонсировали то, о чём уже давно говорили: чип собственного производства Apple M1.

В этом посте я расскажу о том интересном, что заметил за время первой недели использования MacBook, свои личные впечатления.  

Я не буду рассказывать о том, что и так все знают из интернета: что отсутствует вентилятор, как изменились производительность и автономность, как дизайн отличается от Windows и какие есть проблемы с [совместимостью с M1](https://isapplesiliconready.com/) у некоторых приложений. Здесь будет только о том, что мне лично показалось интересным или неожиданным.

### Trackpad

Я довольно редко работал без мыши, поэтому всегда удивлялся, как это ловко получается у других людей. За «трекпад» (вне macOS — «тачпад») я немного опасался, считал, что он может быть одной из причин (наряду с небольшим экраном), почему MacBook может не понравиться, хотя купить мышь конечно никто не мешает, а монитор я уже подключил.  

На самом деле всё оказалось не просто не так страшно, а наоборот — очень и очень круто. Ко всем этим [жестам](https://support.apple.com/ru-ru/HT204895) привыкаешь уже на второй день и начинаешь инстинктивно пытаться использовать их на старом ПК. Жесты предусмотрены на самые разные случаи, даже на случай, если вы потеряли курсор на экране (можно резко подвигать пальцем по трекпаду, чтобы курсор увеличился и стал заметным). Очень удобно перелистывать страницы в браузере и скроллить двумя пальцами. Отсутсвие мыши абсолютно незаметно, возможно я о ней вспомню, если захочу поиграть в CS :P. 

### Джин

Наверное одним из самых первых действий в плане настроек было отключить так называемый [эффект Джина](https://www.reddit.com/r/funny/comments/1n7cfw/still_the_best_use_of_the_mac_genie_effect/) (Genie Effect ) при сворачивании окна. У мака есть много крутых вещей в плане дизайне, но, по мне, Джин — точно не одна из них. Кроме сомнительной красоты, есть ощущение, что Джин сворачивает окна чуточку медленнее, чем обычное сворачивание без эффекта. Может, на какую-то долю секунды. Наверное, когда-то он был модным, но в 2020 году такой анимацией уже никого не удивить.

### Смена раскладки клавиатуры

Сочетание клавиш для смены языка (Command + пробел) перекрывается сочетанием клавиш для вызова Spotlight, о чём даже указано в документации Apple, где как раз и советуют, что 	нужно отключить это сочетание для Spotlight, если вам нужно сочетание для смены языка. Не знаю, почему такое странное поведение, как будто бы мультиязычность — это что-то не особо приоритетное.  

При этом есть клавиша Fn, которая тоже переключает язык (такое поведение, кажется, появилось только с M1). Но очень многие отмечают два момента: первое — неудобно тянутся до клавиши, часто промахиваешься; второе — почему-то не всегда срабатывает переключение. Не очень часто — может быть, в одном случае из 20, но такое точно не только у меня. Видимо, как раз из-за этих двух причин многие настраивают переключение на Command + пробел.

### Numpad

Насколько я знаю, не только Air, но и многие Pro не имеют цифровой клавиатуры (Numpad). Не то чтобы я этим блоком очень часто пользовался, но кое-какие Alt-коды помню, например Alt+0233 для accent aigu é. В маке проблема с диакритическими знаками решается довольно просто: для очень многих букв есть вариации, которые появляются, если нажать на эту букву чуть дольше. Например, в английском для буквы **e** вам будут предложены: è, é, ê, ë, ē, ė, ę. Еще в Numpad были знаки плюс, минус, умножить, Home, End, которые позволяли избегать лишних нажатий на Shift. На маке, увы, придётся привыкать к сочетаниям.
Наверное, из всего этого списка жутко не хватает клавиши **Delete**. Я пока узнал только про её аналог Fn + ⌫.

### Буква Ё

Клавиша для буквы «Ё» расположена практически в противоположном углу от того, где располагалась на ПК Windows. Думал, что буду долго привыкать, но самом деле оказалось довольно удобно. Кажется, такое было не всегда — в интернете есть посты прошлых лет о том, как напечатать букву «ё», видимо для неё не было отдельной клавиши.

### Точка и запятая в русском

В русской раскладке точка ставится нажатием Shift + 5, запятая — Shift + 6. Причём в английском всё располагается ожидаемо справа внизу. Зачем они так сделали? Привыкнуть на самом деле можно. Но, по-моему, если часто используешь два языка (как у меня на работе), то такое разное расположение не очень удобно. При этом, вопросительный знак в русском тоже справа внизу. А восклицательный знак — над 1, то есть как во многих ПК. «Кавычки-ёлочки» ставятся Shift+Option+= и Option+=, либо я пока просто не нашёл более простого способа (это если не включать автозамену "таких кавычек", так как оно не всегда нужно). 

### Вырезать файл

Нет пункта **Вырезать файл** в контекстном меню! Как так? В стандартном приложении Pages есть опция и для копирования, и для вырезания текста, а вот для файлов в Finder — нет. Можно только скопировать, а потом вставить либо копию, либо сам объект.  
Сочетание клавиш для вставки вырезанного файла: ⌥ + ⌘ + V, где ⌥ — это "Option", а ⌘ — это "Command", приятно познакомиться.

### Ctrl+Z и Ctrl+Y (Undo & Redo)

Многие основные сочетания клавиш привычны: Command-F — поиск, Command-A — выделить всё, Command-S — сохранить.  
Поэтому удивительным казалось, что хотя Command-Z приводит к отмене последнего действия (Undo), Command-Y при этом не приводит к отмене этой отмены (Redo). Для этого нужно нажать Command-Shift-Z, что не только непривычно, но и очень неудобно.  
Сначала я думал, что это проблема редактора [Coteditor](https://coteditor.com), в котором я набираю этот текст. Там нажатие Command-Y печатало знак йены/юаня **¥**. Странная фича, возможно, связана с тем, что разработчики из Японии. Потом увидел, что в стандартном Pages сочетание Command-Y вообще не приводит ни к чему. Ну что ж, ладно, будем привыкать к новому сочетанию.  
Кстати, Command-Z также работает как "открыть последнее закрытое окно" в Safari.

### Support  и справка

Может, для остальных это и не очень интересный момент, но мне как техписателю показался довольно важным.  
Обычно бывает так: ищешь описание функции или вопрос в гугле, попадаешь на какой-нибудь форум или Stack Overflow, и там люди что-то подсказывают. В случае с маком большая часть запросов вела на сайт поддержки Apple, который содержал ответы на все вопросы. Хотя не исключаю и того, что у меня, как новичка, они были относительно простые, и когда начнутся сложные, не всё можно будет найти на официальном сайте. Иногда по знаку вопроса в окнах MacBook открывал встроенную справку, и она оказывалась на удивление полезной. Не помню вообще, чтобы когда-то читал справку Windows.

### Работа с окнами

Для тех, кто переходит с Windows, есть сразу несколько непривычностей:

* Сворачивание окна не происходит по нажатию на иконку программы в Dock. В Ubuntu работало так же, но там можно было настроить и «виндовое» поведение. В мак для сворачивания надо либо нажать на кнопку сворачивания (слева вверху), либо сочетание клавиш Command + M. Можно также настроить, чтобы сворачивание происходило по нажатию на заголовок окна, но это не сильно ближе для курсора, чем кнопка, разве что требует меньшей прицельности.  Кроме того, по умолчанию окна сворачиваются в отдельную область в правой части Dock (аналог панели задач). Это поведение можно поменять, сняв галочку «Убирать окно в Dock в значок приложения» (Minimize windows into application icon).

* Название запущенного в текущий момент приложения и основные его параметры отображаются в Меню приложения (**App menu**). По началу было немного непривычно, что у всех приложений его по сути стандартизированное меню.

* Закрытие окна приложения не приводит к завершению его работы. Черной точкой в Dock помечаются запущенные приложения. И если вы просто закрываете окно приложения, например браузер, это не означает, что приложение тоже остановлено. В Windows гораздо менее очевидно, что некоторые приложения остаются запущенными в фоне. Если вам действительно нужно завершить приложение, есть как минимум [шесть способов](https://appleinsider.ru/mac-os-x/6-sposobov-zakryt-prilozhenie-na-mac.html), из которых самое простое — вызвать контекстное меню в Dock, нажав по приложению двумя пальцами. Запуск приложения обычно открывает все окна, которые были открыты в момент завершения. Это поведение тоже [можно поменять](https://support.apple.com/ru-ru/HT204005). 

### Предустановленные приложения
Несвязанный набор первых опробованных приложений:
* Пока я отвечал на какой-то звонок, первым приложением, которое запустила моя жена сразу после распаковки мака, было Photo Booth, в котором мы потом прикалывались с помощью разных эффектов.
* Аналоги «офисных приложений»: Word → Pages, Excel → Numbers, PowerPoint → Keynote.
* Наверное, в Калифорнии все очень любят играть на музыкальных инструментах, потому что по умолчанию на маке установлено приложение Garage Band, с фонотекой на несколько гигов.  
* Есть одна предустановленная игра — шахматы.
* Интересным показалось приложение "Подкасты", там даже есть записи проекта [Arzamas](https://arzamas.academy).
* Есть некий "Редактор скриптов", который служит для создания скриптов на [Apple Script](https://developer.apple.com/library/archive/documentation/AppleScript/Conceptual/AppleScriptLangGuide/introduction/ASLR_intro.html) или JavaScript for Automation. Если кратко, то это язык для скриптов, которые могут взаимодействовать непосредственно с приложениями macOS. Например, я тут же попробовал создать скрипт, который при запуске открывал бы Safari и переходил в нём на сайт https://www.duolingo.com.   
* Есть ещё AppleTV, который манит меня бесплатной подпиской на год.  
* Понравилось приложение Maps («Карты»), в котором для некоторых городов есть функция Flyover, она позволяет как бы пролететь над достопримечательностями с высоты птичьего полёта, очень крутые кадры!  
* По умолчанию установлен «Дом», чтобы вы как можно скорее приобрели что-нибудь из [Apple HomeKit](https://www.apple.com/ru/shop/accessories/all/homekit?cid=APP-RU-Home-Landing-List).

### Никаких венти­ляторов

Это уже особенность не операционной системы, а именно MacBook Air на M1: в них нет вентиляторов, они абсолютно бесшумны. Как пишет Apple, "No fan. No noise. Just Air.". 

К этому очень быстро привыкаешь. И вспоминаешь об этой разнице, только когда включаешь какой-нибудь другой компьютер, и он периодически гудит, напоминая о кулере. Конечно, есть и другие ноутбуки (или "ультрабуки") с пассивным охлаждением.

### Периферийные устройства
Когда речь заходить о MacBook, всегда вспоминаются все эти истории с разъёмами и портами, мол «старых» USB тут нет, HDMI и пр. тоже нет, есть только два Type-C порта, оба слева. Поэтому владельцам MacBook практически всегда приходится покупать адаптеры, если требуется использоваться флешки или подключать монитор. Или же стараться использоваться только беспроводные устройства. Мне тоже пришлось приобрести переходник. Зато само подключение прошло без единой проблемы. В итоге к MacBook получилось подключить:
* Дешёвые Bluetooth-наушники Sony WHCH400;
* Не самые дешёвые Bluetooth-наушники Beats Solo3 Wireless;
* Разветвитель Type-C Lyambda Slim Aluminum LC173 со входами: 3 × USB 3.0 Type-A, 1 × HDMI, 1 × USB 3.1 Type-C;
* Монитор Benq через этот самый Lyambda.


