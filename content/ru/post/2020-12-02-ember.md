---
title: Ember.js
subtitle: That I liked
date: 2020-12-01
tags: ["example"]
draft: true
---

Follow [this guide](https://guides.emberjs.com/release/getting-started/quick-start/).

"Your option is Ember if you like Ember."

<!--more-->

1. Tomster.
2. Style

	
```
gitpod /workspace/ember-pod $ ember -v
ember-cli: 3.22.0
node: 12.20.0
os: linux x64
```
```
Congratulations, you made it!
You’ve officially spun up your very first Ember app :-)
```

> The Ember brand and associated assets are part of a meticulously curated experience.

Одной из эмблем фреймворка является [Хомяк Томстер](https://emberjs.com/mascots).