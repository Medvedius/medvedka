---
title: Ruby on Rails на Сodeanywhere
subtitle:
date: 2020-11-26
tags: ["example"]
---

В книге **Ruby on Rails Tutorial** Майкл Хартл (автор серии книг [Learn enough to be dangerous](https://www.learnenough.com/courses)) приводит пример развертывания приложения в инфраструктуре Cloud9 от Amazon Web Services (AWS).
Эта штука не бесплатная, поэтому я захотел найти им альтернативу и обратил внимание на [Сodeanywhere](https://codeanywhere.com/), созданную нашими братьями-славянами хорватами Ivan Burazin и Vedran Jukic. Жаль конечно, что в итоге бесплатный период у них оказался всего 7 дней ☺. Но зато ради этих 7 дней не потребовались вводить данные банковской карточки.

Чтобы начать использовать его, нужно завести учетную запись. После этого первым этапом является выбор "контейнера", то есть виртуального сервера, подстроенного под конкретный стек технологий (например, LAMP, Python, C/C++, NodeJS), каждый из которых доступен на одной из двух ОС: Ubuntu 16.04 или CentOS 7.2.
Очень захотелось попробовать CentOS, но знакомство с Сodeanywhere решил начать с более привычной Ubuntu.  

Выбрал контейнер **Ruby Development Stack with RVM and Ruby on Rails preinstalled**.
Сервер выдает вот такую конфигурацию:
```ssh
2GB of Disc Storage
256MB RAM (+ 512 MB swap)
Sudo access
SSH access on host26.codeanyhost.com -p 58369
Access to all HTTP and Websocket ports
```

Проверяем, что же там имеется.  
Ruby?
```
cabox@RubyOnUbuntu:~/workspace$ ruby -v
ruby 2.5.1p57 (2018-03-29 revision 63029) [x86_64-linux]
```
SQLite3?
```
cabox@RubyOnUbuntu:~/workspace$ sqlite3 --version
3.11.0 2016-02-15 17:29:24 3d862f207e3adc00f78066799ac5a8c282430a5f
```
Нода?
```
cabox@RubyOnUbuntu:~/workspace$ node --version
v8.12.0
```
Yarn?
```
cabox@RubyOnUbuntu:~/workspace$ yarn -v
1.10.1
```
И Rails?
```
cabox@RubyOnUbuntu:~/workspace$ rails --version
Rails 5.2.4.4
```
Конечно, сказали же, что всё preinstalled.  
Версия Rails 5.2.4.4 вышла в сентябре 2020, не очень-то и старая версия, хотя есть уже и 6.0.
В [Quick Tutorial](https://docs.codeanywhere.com/#quick-tutorial) от Codeanywhere приводится пример, где они запускают сервер буквально сразу после создания первого файла “index.php”.
У меня так не получилось, нужно не просто создать первое приложение на Ruby on Rails, но и зайти в конфигурацию контейнера, чтобы:
+ Проверить рабочий каталог. В моём случае понадобилось добавить `blog` к пути по умолчанию, чтобы получилось ` "cwd": "~/workspace/blog",`.
+ Проверить "Run commands list". Закомментируем `"echo 'Run configuration not defined!'`, раскомментируем ` "rails server"` и удалим после него запятую.
+ Проверить, что в Environment variables указан порт 3000.

Вот теперь, если нажать кнопку "Run", вылезет стандартная Rails'овская картинка с надписью **Yay! You're on Rails**.

В остальном — очень простой, красивый и удобный интерфейс. Показалось попроще gitpod. Поэтому очень жаль, что пробный период всего 7 дней, причем из них ты можешь пользоваться программой только 2 часа.