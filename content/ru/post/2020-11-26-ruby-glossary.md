---
title: Speak Ruby
subtitle: main terms
date: 2020-11-26
tags: ["ruby"]
---

На этой странице я буду собирать основные понятия, термины и библиотеки, связанные с Ruby.
Глоссарий **не** включает в себя термины Ruby как языка программирования (такие как Hash или Symbol). Он имеет целью собрать ключевые слова из того, что окружает Ruby: его сообщества, экосистемы, интересных библиотек.

[//]: # ({{< tweet 1333541312897826817 >}})

Gem
: Название формата библиотек Ruby, расширяющих его функциональность. Управляются менеджером пакетов [RubyGems](https://rubygems.org/pages/download). Установка пакета осуществляется командой `gem install <название>`. Самые популярные gems — [тут](https://rubygems.org/stats).

Ruby on Rails
: Популярнейший фреймворк, название которого обычно одним из первых всплывает в памяти при упоминаии Ruby. Создан датчанином [Давидом Хейнемейером Ханссоном](https://twitter.com/dhh) в 2004 году. На его основе разработаны, например, GitHub, GitLab, Airbnb, Shopify, Hulu.

Bundler
: Инструмент управления gem-файлами и их зависимостями для конкретного Ruby-проекта.

Gemfile
: Основной файл, используемый Bundler для управления версиями и зависимостями в проекте Ruby.

RDoc
: Инструмент генерации документации на основе комментариев исходного кода. Выходные форматы: HTML и `ri`.

ri
: Справочная система Ruby, позволяющая получить в консоли информацию о классе, модуле, библиотеке и т.п.: `ri MyClass`

why's poignant guide to ruby
: Необычный учебник по Ruby, написанный Jonathan Gillette, известным под псевдонимом *why the lucky stiff* (сокращенно — *_why*). Книга породила кучу мемов в Ruby-сообществе, таких как Cartoon Foxes и chunky bacon.

Ruby koans
: Программа, призванная обучить Ruby с помощью тестирования и выполнения несложных заданий. Создана Jim Weirich (1956 – 2014), одним из самых активных разработчиков и популяризаторов Ruby, создавшим Rake, RubyGems и пр.

Rake
: Одна из стандартных библиотек Ruby для запуска задач (например, автоматизации сборки) и настройки зависимостей задач. Создана Jim Weirich как аналог Make (из языка C). Задачи описываются в файле Rakefile (аналог [Makefile](https://en.wikipedia.org/wiki/Makefile)).

Rack
: Программа, служащая интерфейсом между веб-сервером и веб-приложением. Rack оборачивает HTTP-запросы и ответы и упрощает API.

Ruby Toolbox
: Простой по задумке, но удивительно удобный [сайт](https://www.ruby-toolbox.com/), на котором собрана информация об очень многих библиотеках для Ruby. Для поиска библиотеки есть возможность выбрать категорию, например Code Quality, Documentation Tools, Testing. Для каждой библиотеки
показывается такая информация как количество скачиваний, звёзд на GitHub, а также проставляется статус типа "Многлетний стабильный проект" или "Не было релизов уже три года" или "Очень много открытых багов".

Rubocop
: Анализатор кода ([линтер](https://en.wikipedia.org/wiki/Lint_(software))), который проверяет Ruby-программу на соответствие [стайлгайду Ruby](https://rubystyle.guide/). Rubocop создан болгарином [Bozhidar Batsov](https://github.com/bbatsov).

kramdown
: Парсер Markdown-файлов ("probably the fastest pure-Ruby Markdown converter").

Homebrew
: Программа, написанная на Ruby, для установки пакетов и приложений на macOS и Linux.

Rouge
: Инструмент для подсветки синтаксиса кода в тексте.

Jekyll
: Максимально простой в использовании генератор статических сайтов, написанный на Ruby [Tom Preston-Werner](https://github.com/mojombo) (один из создателей GitHub) и [Parker Moore](https://github.com/parkr) в 2008 году.

Sinatra
: Микрофреймворк для веб-приложений, трудно сравнивать с Rails, так как намного проще, легче и подходит для других задач.

Puma
: Веб-север на Ruby, по умолчанию входящий в Ruby on Rails.

ERB
: Язык шаблонов Ruby для генерации веб-страниц (HTML, XML). См. также [Liquid](https://shopify.github.io/liquid/) и [Haml](https://haml.info/).
